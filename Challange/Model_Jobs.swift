//
//  Model_JobsJobs.swift
//  Challange
//
//  Created by appentus technologies pvt. ltd. on 10/11/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.
//

import Foundation

class Model_Jobs {
    static let shared = Model_Jobs()
    
    var jobId = ""
    var category = ""
    var postedDate = ""
    var status = ""
    var total_Hired = ""
    
    var arr_connectedBusinesses = [Model_Jobs]()
    var arr_Jobs = [Model_Jobs]()
    
    func func_Jobs(completionHandler:@escaping (Bool)->()) {
        if let path = Bundle.main.path(forResource: "jobs", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? Dictionary<String, AnyObject>, let jobs = jsonResult["jobs"] as? [[String:Any]] {
                    print(jobs)
                    
                    for job in jobs {
                        self.arr_Jobs.append(self.func_set_data(job))
                    }
                    completionHandler(true)
                }
            } catch {
                completionHandler(false)
            }
        }
    }
    
    private func func_set_data(_ dict:[String:Any]) -> Model_Jobs {
        let model = Model_Jobs()
        
        model.jobId = "\(dict["jobId"] ?? "")"
        model.category = "\(dict["category"] ?? "")"
        model.status = "\(dict["status"] ?? "")"
        model.postedDate = "\(dict["postedDate"] ?? "")"
        
        if let arrConnectedBusinesses = dict["connectedBusinesses"] as? [[String:Any]] {
            for dictConnectedBusinesses in arrConnectedBusinesses {
                model.arr_connectedBusinesses.append(self.func_set_data_connectedBusinesses(dictConnectedBusinesses))
            }
        }
        
        return model
    }
    
   var businessId = ""
   var thumbnail = ""
   var isHired = false
    
    private func func_set_data_connectedBusinesses(_ dict:[String:Any]) -> Model_Jobs {
        let model = Model_Jobs()
        
        model.businessId = "\(dict["businessId"] ?? "")"
        model.thumbnail = "\(dict["thumbnail"] ?? "")"
        model.isHired = dict["isHired"] as! Bool
        
        return model
    }
    
}


