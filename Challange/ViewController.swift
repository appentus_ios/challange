//  ViewController.swift
//  Challange
//  Created by appentus technologies pvt. ltd. on 10/11/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.



import UIKit
import SDWebImage



class ViewController: UIViewController {
    @IBOutlet weak var lead_yellow : NSLayoutConstraint!
    @IBOutlet weak var tbl_jobs : UITableView!
    
    var arr_ConnectPeople = [Model_Jobs]()
    
    var arr_Close = [Bool]()
    
    var is_LoadJson = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Model_Jobs.shared.func_Jobs { (status) in
            self.is_LoadJson = true
            for _ in 0..<Model_Jobs.shared.arr_Jobs.count {
                self.arr_Close.append(false)
            }
            self.tbl_jobs.reloadData()
        }
        
    }
    
    @IBAction func btn_openJobs (_ sender:UIButton) {
        self.lead_yellow.constant = 0

        UIView.animate(withDuration:0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btn_closedJobs (_ sender:UIButton) {
        self.lead_yellow.constant = self.view.bounds.width/2

        UIView.animate(withDuration:0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
}



extension ViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = Model_Jobs.shared.arr_Jobs[indexPath.row]
        if model.arr_connectedBusinesses.count == 0 {
            return 250
        }
        return 340
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if is_LoadJson {
            return Model_Jobs.shared.arr_Jobs.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = TableViewCell()
        let model = Model_Jobs.shared.arr_Jobs[indexPath.row]
        
        if model.arr_connectedBusinesses.count == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "cell-2", for: indexPath) as! TableViewCell
            
            arr_ConnectPeople = model.arr_connectedBusinesses
            cell.collConnectedBusinesses.delegate = self
            cell.collConnectedBusinesses.dataSource = self
            
            cell.collConnectedBusinesses.reloadData()
        }
        
        var total_Hired = 0
        for modelHired in model.arr_connectedBusinesses {
            if modelHired.isHired {
                total_Hired += 1
            }
        }
        
        cell.lbl_Electricians.text = model.category
        cell.lbl_Posted_Date.text = model.postedDate
        cell.lbl_InProgress.text = model.status
        
        if total_Hired == 0 {
            cell.lbl_ConnectingYouWithBusiness.text = "Connecting you with business"
        } else {
            cell.lbl_ConnectingYouWithBusiness.text = "You have hired \(total_Hired) businesses "
        }
        
        if arr_Close[indexPath.row] {
            cell.width_close.constant = 150
        } else {
            cell.width_close.constant = 0
        }
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
        cell.btn_more.tag = indexPath.row
        cell.btn_more.addTarget(self, action: #selector(btn_More(_:)), for: .touchUpInside)
        
        cell.btn_close.tag = indexPath.row
        cell.btn_close.addTarget(self, action: #selector(btn_More(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func btn_More(_ sender:UIButton) {
        for i in 0..<Model_Jobs.shared.arr_Jobs.count {
            if i == sender.tag {
                if arr_Close[i] {
                    arr_Close[i] = false
                } else {
                    arr_Close[i] = true
                }
            } else {
                arr_Close[i] = false
            }
        }
        tbl_jobs.reloadData()
    }
    
}



//  MARK:- UICollectionView methods
extension ViewController:UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = 60 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 16 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize (width: 60, height:65)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr_ConnectPeople.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        let img = cell.viewWithTag(1) as! UIImageView
        let lbl_hired = cell.viewWithTag(2) as! UILabel
        let view_theme = cell.viewWithTag(3) as! UIView
        
        img.layer.cornerRadius = img.frame.size.height/2
        img.clipsToBounds = true
        
        lbl_hired.layer.cornerRadius = lbl_hired.frame.size.height/2
        lbl_hired.clipsToBounds = true
        
        
        view_theme.layer.cornerRadius = view_theme.frame.size.height/2
        view_theme.clipsToBounds = true
        
        let model = arr_ConnectPeople[indexPath.row]

        img.sd_setImage(with:URL (string:model.thumbnail), placeholderImage:(UIImage(named:"images.jpeg")))

        if model.isHired {
            lbl_hired.isHidden = false
        } else {
            lbl_hired.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}


