//
//  TableViewCell.swift
//  Challange
//
//  Created by appentus technologies pvt. ltd. on 10/11/19.
//  Copyright © 2019 appentus technologies pvt. ltd. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var view_container:UIView!
    @IBOutlet weak var btn_more:UIButton!
    @IBOutlet weak var btn_close:UIButton!
    @IBOutlet weak var collConnectedBusinesses:UICollectionView!
    @IBOutlet weak var width_close:NSLayoutConstraint!
    
    
    @IBOutlet weak var lbl_Electricians:UILabel!
    @IBOutlet weak var lbl_Posted_Date:UILabel!
    @IBOutlet weak var lbl_InProgress:UILabel!
    @IBOutlet weak var lbl_ConnectingYouWithBusiness:UILabel!
    @IBOutlet weak var btn_ViewDetails:UIButton!
    
//    @IBOutlet var img_person: [UIImageView]!
//    @IBOutlet var lbl_hired: [UILabel]!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
            
        btn_close.layer.cornerRadius = 2
        btn_close.layer.borderColor = hexStringToUIColor("ADABA9").cgColor
        btn_close.layer.borderWidth = 1
        btn_close.clipsToBounds = true
        
        view_container.layer.cornerRadius = 2
        view_container.layer.borderColor = hexStringToUIColor("ADABA9").cgColor
        view_container.layer.borderWidth = 1
        view_container.clipsToBounds = true
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}





func hexStringToUIColor (_ hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
